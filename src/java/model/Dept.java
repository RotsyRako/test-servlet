package model;

import annotation.Path;
import java.util.HashMap;
import util.ModelView;

/**
 *
 * @author Rotsy - ETU001448
 */

public class Dept {
    private String nom;

    public Dept() {
        this.nom = "ROTSY";
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    @Path(url="testPage")
    public ModelView showPage(){
        Dept d = new Dept();
        ModelView mv = new ModelView();
        mv.setTarget("testPagerr.jsp");
        HashMap<String, Object> attribute = new HashMap<>();
        attribute.put("obj", d);
        mv.setAttribute(attribute);
        return mv;
    } 
}
