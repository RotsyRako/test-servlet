package model;

import java.util.List;
import util.ModelView;

/**
 *
 * @author Rotsy - ETU001448
 */
public class Emp {
    private String nom;
    private int age;
    private String poste;

    public Emp() {
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPoste() {
        return poste;
    }

    public void setPoste(String poste) {
        this.poste = poste;
    }
    
    public ModelView insertionEmp(Emp e){
        ModelView mv = new ModelView();
        
        return mv;
    }
    
//    public List<Emp> listEmp(){
//        
//    }
}
