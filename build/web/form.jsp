<%-- 
    Document   : form
    Created on : 18 nov. 2022, 22:45:36
    Author     : Rotsy - ETU001448
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sprint 3</title>
    </head>
    <body>
        <h1>Inscrire un employ&eacute;</h1>
        <form action="insertionEmp.do" method="POST">
            Nom <input type="text" name="nom">
            Age <input type="number" name="age" min="0">
            Poste <input type="text" name="poste">
            <input type="submit" value="insert">
        </form>
    </body>
</html>
